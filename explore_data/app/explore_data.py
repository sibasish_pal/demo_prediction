import os
import sys
import types
import pickle
import marshal
import warnings
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
from xgboost import XGBRegressor
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_data = pickle.load(open(f"{PICKLE_PATH}/train_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    test_data = pickle.load(open(f"{PICKLE_PATH}/test_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = explore_data
## $xpr_param_component_type = pipeline_job 
## $xpr_param_global_variables = ["train_data","test_data"]
train_data.info()
# Function to get a list of the variables that contain missing values and plot by passing the dataframe


def missingValuesInfo(df,plot=False):
    total = df.isnull().sum().sort_values(ascending = False)
    percent = round(total/len(df)*100, 2)
    temp = pd.concat([total, percent], axis = 1,keys= ['Total', 'Percent'])
    
    if sum(total) == 0:
        print("No missing values present!")
        return temp.loc[(temp['Total'] > 0)]
    
    if plot:
        f, ax = plt.subplots(figsize=(15, 6))
        plt.xticks(rotation='90')
        sns.barplot(x=temp.index, y=temp['Percent'])
        plt.xlabel('Features', fontsize=15)
        plt.ylabel('Percent of missing values', fontsize=15)
        plt.title('Percent missing data by feature', fontsize=15)
        save_fig("missing_Values_Info")
    return temp.loc[(temp['Total'] > 0)]
# Check missing values in training and test dataset
#check_missing_values(train_data)
missingValuesInfo(train_data,True)

# Property type has almost 15% of rows with missing values
# Address has almost 4% of rows with missing values
# County and zipcode has less than 0.50% of rows with missing values

# Based on understanding different columns. The missing values possibly happened either due to not recorded or not available.
# county        - Not recorded
# zipcode       - Not recorded
# address       - Not recorded
# property_type - Not recorded
#check_missing_values(test_data)
missingValuesInfo(test_data,True)
train_data.describe()
sns.barplot(x=train_data.property_type,y=train_data.rent);
save_fig("Property_type_bar_plots")
sns.countplot(x=train_data.property_type)
save_fig("Property_type_count_plots")
def correlation1(dataset, threshold):

    d = [] # Create an empty list to store the relations
    
    corr_matrix = dataset.corr() # Get correlation matrix
    
    # Loop through each columns in dataset.
    for i in range(len(corr_matrix.columns)):
        
        # Getting the name of column
        colname = corr_matrix.columns[i]
        
        # Loop through each rows untill range of column
        for j in range(i):
            if abs(corr_matrix.iloc[i, j]) >= threshold: # we are interested in absolute coeff value
                rowname = corr_matrix.index[j]
                d.append([colname,rowname])
                
    return d
# Calculate correlation matrix
corrmat = train_data.corr()

# Plot a heatmap to understand the correlations between different features and also with target variable.
f, ax = plt.subplots(figsize=(20, 20))

sns.heatmap(corrmat, annot=True,cbar=True, fmt='.2f', cmap=plt.cm.CMRmap_r)
save_fig("correlation_matrix_plot")
# Plot heatmap of Top 10 correlated features with target variable(rent)
cols = corrmat.nlargest(11, 'rent')['rent'].index
cm = np.corrcoef(train_data[cols].values.T)
plt.figure(figsize=(20, 10))
sns.heatmap(cm, vmax=.8, linewidths=0.01,
            square=True,annot=True,cmap='YlGnBu',linecolor="white", yticklabels=cols.values, xticklabels=cols.values)
save_fig("correlation_top10_plot")
plt.title('Correlation between top 10 features');
top_corr = pd.DataFrame(cols)
top_corr.columns = ['Most Correlated Features']
top_corr
train_data.drop('rent',axis=1).corrwith(train_data.rent).plot.bar(figsize = (20, 10), 
                                       title = "Feature's Correlation with Rent", fontsize = 15,
                                       rot = 90, grid = True);
save_fig("features_correlation_plot")
corr_col_todrop = correlation1(train_data.drop('rent',axis=1), 0.50)
print(corr_col_todrop)
#Check the distribution of all the independent variables
train_data.hist(bins=50,color='blue',figsize=(32,40), layout=(8,4))
save_fig("distribution_independent_histplot")
xpresso_save_plot("image_1", output_path=os.environ['XPRESSO_MOUNT_PATH'],output_folder="xjp_images/explore_data")
#sns.boxplot(x=corr_Xtrain['property_type'])
train_data.plot(kind='box',subplots=True, figsize=(25,50), layout=(8,4))
save_fig("outlier_plot")
xpresso_save_plot("image_2", output_path=os.environ['XPRESSO_MOUNT_PATH'],output_folder="xjp_images/explore_data")

# No outliers present for property_type, zipcode
# bed, bath - needs to check outlier removal performance
# garage, pool, fireplace, patio have just two values. Hence, cannot be considered as outlier
# Also halfbath can be ignored, while checking for outliers as it has just 7 unique values.
# ['property_type', 'zipcode', 'garage', 'pool', 'fireplace', 'patio'] - Skipped from outlier removal
train_data.plot(kind='kde', title="Original Data",figsize=(15,9))
save_fig("scatter_matrix_plot")
# Plot histogram to graphically show skewness and kurtosis

plt.figure(figsize=[15,5])
sns.distplot(train_data.rent)
save_fig("skewness_plot1")
plt.title('Distribution of Rent')
plt.xlabel('Rent')
plt.ylabel('Number of Occurences')

# normal probability plot
plt.figure(figsize=[8,6])
stats.probplot(train_data.rent, plot=plt)
save_fig("skewness_plot2")

# Calculate skewness and kurtosis values of target variable (rent)
print("Skewness: %f" % train_data.rent.skew())
print("Kurtosis: %f" % train_data.rent.kurt())

#Output_below
#Skewness: 3.391721
#Kurtosis: 23.032647

# Plot histogram to graphically show skewness and kurtosis

plt.figure(figsize=[15,5])
sns.distplot(np.log(train_data.rent))
plt.title('Distribution of Rent')
plt.xlabel('Rent')
plt.ylabel('Number of Occurences')

# normal probability plot
plt.figure(figsize=[8,6])
stats.probplot(np.log(train_data.rent), plot=plt)

# Calculate skewness and kurtosis values of target variable (rent)
print("Skewness: %f" % np.log(train_data.rent).skew())
print("Kurtosis: %f" % np.log(train_data.rent).kurt())

#Output_below
#Skewness: 0.546390
#Kurtosis: 0.617851

try:
    pickle.dump(train_data, open(f"{PICKLE_PATH}/train_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(test_data, open(f"{PICKLE_PATH}/test_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

